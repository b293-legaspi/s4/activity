package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication

@RestController

public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);


	}

	@GetMapping("/hello")

	public String hello(@RequestParam(value="name", defaultValue="World") String name){
		return String.format("Hello %s",name);
	}
	@GetMapping("/greetings")
	public String greetings(@RequestParam(value="greet", defaultValue="World") String greet){
		return String.format("Good evening %s! Welcome to batch 293!",greet);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="user", defaultValue="user") String hi){
		return String.format("hi %s!",hi);
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value="name", defaultValue="user") String name, @RequestParam(value="age", defaultValue="18") String age){
//		return String.format("hi" + name + ". Your age is " + age);
		return String.format("hi %s. Your age is %s", name, age);
	}

}
